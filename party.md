# Party

| Name                      | Race      | Class      | Spec        | AC | PP | Role                 | Player  | Profile |
|---------------------------|-----------|------------|-------------|----|----|----------------------|---------|---------|
| Pinocchio                 | Warforged | Druid      | Moon        |    |    | Dread Pirate Roberts | Jim     | ![Profile](/images/pinocchio.png "Profile")        |
| Rylie 'Mad-Eyes' Goodwin  | Kenku     | Rogue      | Swashbuckler|    |    |                      | Matthew | ![Profile](/images/matthew.png "Profile")        |
| Swain                     | Kenku     | Artificer  | Artillerist |    |    | Cannoneer            | Michael | ![Profile](/images/swain.jpg "Profile")        |
| Quaidarius                | Dwarf     | Fighter    |             |    |    | Brewer               | Alex    | ![Profile](/images/quaidarius.jpeg "Profile")        |
| Pitaya Dragotini          | Pianta    | Warlock    | Fathomless  |    |    |                      | Cameron | ![Profile](/images/pinky.jpg "Profile")        |
| Elaina Lavere             | Half-elf  |            |             |    |    | Seamist Mercantile   | Cindy   |         |
| Elred Carroa              | Half-elf  |            |             |    |    | Tavern owner         | Greg    |         |

# Crew
| Name                      | Profile |
|---------------------------| ------- |
| Mr. Buttons & Kirk        | <img src="images/mrbuttons.png" height="292" />


# Inventory

## Banana Boat

See [schooner ship](schooner.md) for details.

### Storage
- rowboat
