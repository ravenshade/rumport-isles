# The Desert Lynx

Hull | Helm | Sails
-----|------|------
450/450 | 75/75 | 150/150|

# Sloop Ship

*Large vehicle (80 ft. by 20 ft.)*

**Creature Capacity** 32-80 crew, 20 passengers. Requires 5-6 crew.

**Cargo Capacity** 60 tons. 8-12 cannon capacity.

**Travel Pace** 6 miles per hour

**Speed** 60 ft.

**STR**|**DEX**|**CON**|**INT**|**WIS**|**CHA**
:-----:|:-----:|:-----:|:-----:|:-----:|:-----:
20 (+5)|7 (-2)|17 (+3)|0 (-5)|0 (-5)|0 (-5)

**Damage Immunities** poison, psychic

**Condition Immunities** blinded, charmed, deafened, exhaustion, frightened, incapacitated, paralyzed, petrified, poisoned, prone, stunned, unconscious

## Actions

On its turn, the ship can any of the actions below assuming each position has crew. 

### Hull
**Armor Class** 15

**Hit Points** 450 (damage threshold 15)

### Control: Helm
**Armor Class** 18

**Hit Points** 75

Move up to the speed of its sails, with one 90-degree turn. If the helm is destroyed, the ship can't turn.

### Movement: Sails
**Armor Class** 12

**Hit Points** 150; -10 ft. speed per 30 damage taken

**Speed (water).** 45 ft.; 15 ft. while sailing into the wind; 60 ft. while sailing with the wind.
